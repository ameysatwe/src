var a11:number;
var b11:number;
var c11:number;

//show the Calculator
function showCalc()
{
    var tab : HTMLElement = <HTMLElement> document.getElementById('calc');
    tab.style.display = "inline-block";
}
 
//CALCULATOR
//variables for sin,cos,tan,sqrt
var n11:HTMLInputElement=<HTMLInputElement>document.getElementById("n11");
var n12:HTMLInputElement=<HTMLInputElement>document.getElementById("n12");

//FOR SINE FUNCTION
function sin1()
{
    var a:number=Math.PI*parseFloat(n11.value)/180;
    var b:number=Math.sin(a);
    n12.value=b.toString();
}

//FOR COSINE FUNCTION
function cos1()
{
    var c:number=Math.PI*parseFloat(n11.value)/180;
    var d:number=Math.cos(c);
    n12.value=d.toString();
}

//FOR TAN FUNCTION
function tan1()
{   
    var e:number=Math.PI*parseFloat(n11.value)/180;
    var f:number=Math.tan(e);
    n12.value=f.toString();
}

//FOR SQUARE ROOT FUNCTION
function sqrt1()
{
    var g:number=Math.sqrt(parseFloat(n11.value));
    n12.value=g.toString();
}

//to check the current fixed frame with the previous and display Next button
function fixedframecoordinate()
{
    //variables of coordinates of Fixed Frame Coordinate
    let xf:HTMLInputElement=<HTMLInputElement>document.getElementById("xf");
    let yf:HTMLInputElement=<HTMLInputElement>document.getElementById("yf");
    let zf:HTMLInputElement=<HTMLInputElement>document.getElementById("zf");

    //let cood:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("cood");

    a11=parseFloat(xf.value);
    //a11.toFixed(1);
    //console.log(a);
    b11=parseFloat(yf.value);
    //b11.toFixed(1);
    //console.log(b);
    c11=parseFloat(zf.value);
    //c11.toFixed(1);
    //console.log(c);
    
    var matrix1:number[][] = [];
    var matrix2:number[][] = [];

    for(var i=0;i<3;i++){
        matrix1[i] = [];
        for(var j=0;j<3;j++){
            matrix1[i][j] = 0;
        }
    }

    for(var i=0;i<3;i++){
        matrix2[i] = [];
        for(var j=0;j<1;j++){
            matrix2[i][j] = 0;
        }
    }

    matrix2[0][0] = parseFloat(x_m1.value);
    //matrix2[0][0].toFixed(1);
    //console.log("Mob X: " + matrix2[0][0]);
    matrix2[1][0] = parseFloat(y_m1.value);
    //matrix2[1][0].toFixed(1);
    //console.log("Mob Y: " + matrix2[1][0]);
    matrix2[2][0] = parseFloat(z_m1.value);
    //matrix2[2][0].toFixed(1);
    //console.log("Mob Z: " + matrix2[2][0]);
    
    matrix1[0][0] = parseFloat(t1_n.value);
    matrix1[0][1] = parseFloat(t2_n.value);
    matrix1[0][2] = parseFloat(t3_n.value);
    matrix1[1][0] = parseFloat(t4_n.value);
    matrix1[1][1] = parseFloat(t5_n.value);
    matrix1[1][2] = parseFloat(t6_n.value);
    matrix1[2][0] = parseFloat(t7_n.value);
    matrix1[2][1] = parseFloat(t8_n.value);
    matrix1[2][2] = parseFloat(t9_n.value);


    var proc : number[][] = []; 
    for(let i =0;i<3;i++) {
            proc[i] = [];
            for(let j=0;j<1;j++) {
                proc[i][j] = 0;
            }
    }
    for(let i =0;i<3;i++) {
        for(let j=0;j<1;j++) {
            for(let k =0;k<3;k++) {
                proc[i][j] += matrix1[i][k]*matrix2[k][j]
            }
        }
    }

    if( (((proc[0][0]-0.1)<a11) && (a11<(proc[0][0]+0.1))) && (((proc[1][0]-0.1)<b11) && (b11<(proc[1][0]+0.1))) && (((proc[2][0]-0.1)<c11) && (c11<(proc[2][0]+0.1))) )
    {
        alert("Correct Calculation");
        var btn : HTMLInputElement = <HTMLInputElement> document.getElementById('div_7');
        btn.style.display = "inline-block";
        
        x1_l.value="";
        y1_l.value="";
        z1_l.value="";
        
        x1_l.value+=a11;
        y1_l.value+=b11;
        z1_l.value+=c11;
    }
    else{
        alert("Recalculate!");
        var btn : HTMLInputElement = <HTMLInputElement> document.getElementById('div_7');
        btn.style.display = "none";
    }
    

    
    
}

function nextdiv5(){
    var di :  HTMLInputElement = <HTMLInputElement> document.getElementById('sc5');
    di.style.display = "";

}