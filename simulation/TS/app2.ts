var canvas = <HTMLCanvasElement>document.getElementById("mycanvas");
var canvas2 = <HTMLCanvasElement>document.getElementById("mycanvas2");
var context: CanvasRenderingContext2D = canvas.getContext("2d");
var context2: CanvasRenderingContext2D = canvas2.getContext("2d");
var x2:HTMLInputElement=<HTMLInputElement>document.getElementById('M2');
var x1:HTMLInputElement=<HTMLInputElement>document.getElementById('M1');
var a1:HTMLInputElement=<HTMLInputElement>document.getElementById('a');
var y2:HTMLInputElement=<HTMLInputElement>document.getElementById('F2');
var y1:HTMLInputElement=<HTMLInputElement>document.getElementById('F1');
var corr_ang:number; 
var tr:HTMLInputElement=<HTMLInputElement>document.getElementById('rval');
var x11:number;
var y11:number;
var x22:number;
var y22:number;
var l = 100;
var m=25;

var f11:number;
var f22:number;

var x1m:number;
var y1m:number;
var x1f:number;
var y1f:number;
var rm:number;
var anm:number;
var rf:number;
var anf:number;

//var default_axis:string='X';
var t1=0;

// context2.save();
context.translate(canvas.width/2,canvas.height/2);
context.scale(1,-1);
var drawaxes:Geometry.draw_multiple_axis= new Geometry.draw_multiple_axis(canvas,context,t1,'');
drawaxes.draw();
var t=0;

function draw_initial()
{
    //console.log(default_axis);

    var t=0;
    context2.save();
    context2.translate(canvas2.width/2,canvas2.height/2);
    context2.scale(1,-1);
    var drawaxes1 =  new Geometry.draw_multiple_axis(canvas2,context2,t,'');
    drawaxes1.draw();
    context2.restore();
    
}

function next2()
{
    
    context2.clearRect(0,0,canvas2.width,canvas2.height);    
   // context2.clearRect(0,0,canvas2.width,canvas2.height);
   
    var range1 : HTMLInputElement = <HTMLInputElement>document.getElementById("range1");
    var t = parseInt(range1.value);
    tr.value="  angle  :  " + t + "°";
    //console.log(t);
    
    var fy = parseFloat(y2.value);
    
    if(default_axis=='Y'){
        
        x11=+((parseFloat(x1.value)*10)*Math.cos(parseFloat((x2.value))*Math.PI/180));
        //console.log(x11);
        y11=+((parseFloat(x1.value)*10)*Math.sin(parseFloat(x2.value)*Math.PI/180));
        //console.log(y11);
        x22=+((parseFloat(y1.value)*10)*Math.cos((fy+t)*Math.PI/180));
        //console.log(x22);
        y22=+((parseFloat(y1.value)*10)*Math.sin((fy+t)*Math.PI/180));
        //console.log(y22);
        f11 =((parseFloat(y1.value)*10)*Math.cos(parseFloat(y2.value)*Math.PI/180));
        f22 =((parseFloat(y1.value)*10)*Math.sin(parseFloat(y2.value)*Math.PI/180));
    }
    else{
        x11=+((parseFloat(x1.value)*10)*Math.cos(parseFloat((x2.value))*Math.PI/180));
        //console.log(x11);
        y11=+((parseFloat(x1.value)*10)*Math.sin(parseFloat(x2.value)*Math.PI/180));
        //console.log(y11);
        x22=+((parseFloat(y1.value)*10)*Math.cos((fy-t)*Math.PI/180));
        //console.log(x22);
        y22=+((parseFloat(y1.value)*10)*Math.sin((fy-t)*Math.PI/180));
        //console.log(y22);
        f11 =((parseFloat(y1.value)*10)*Math.cos(parseFloat(y2.value)*Math.PI/180));
        f22 =((parseFloat(y1.value)*10)*Math.sin(parseFloat(y2.value)*Math.PI/180));
    }
    
    
    
    
    

    var point_m:number[][]=[];
    var point_f:number[][]=[];
    context2.save();
    context2.translate(canvas2.width/2,canvas2.height/2);
    
    var l = 100;
    var m = 25;
    
   
    if(default_axis=='X')
    {

        //context.scale(-1,1);
        var drawaxes1 =  new Geometry.draw_multiple_axis(canvas2,context2,-t,default_axis);
        drawaxes1.draw();
    
        point_m=get_iso_points(0,x11,y11);
     
        var b:number=point_m[1][0];
    
        var d:number=point_m[0][0];
    
        var drawcircle1=new Geometry.Circle(d,b,10,"red",context2);
        drawcircle1.draw();

        point_f=get_iso_points(0,x22,y22);
        
        var drawcircle2=new Geometry.Circle(point_f[0][0],point_f[1][0],10,"blue",context2);
        drawcircle2.draw();
    }

    else if(default_axis=='Y')
    {

        //context.scale(-1,1);
        var drawaxes1 =  new Geometry.draw_multiple_axis(canvas2,context2,-t,default_axis);
        drawaxes1.draw();

        //fixed
        //console.log("fixed : " + y11 + "  " + x11);
        point_m=get_iso_points(y11,0,x11);
       
        var b:number=point_m[1][0];
    
        var d:number=point_m[0][0];
    
        var drawcircle1=new Geometry.Circle(d,b,10,"red",context2);
        drawcircle1.draw();
        //console.log("mobile : " + y22 + "  " + x22);
        point_f=get_iso_points(y22,0,x22);
    
        var drawcircle2=new Geometry.Circle(point_f[0][0],point_f[1][0],10,"blue",context2);
        drawcircle2.draw();
    }
    
    else if(default_axis=='Z')
    {

        context.scale(-1,1);

        context2.rotate(-Math.PI);
        var drawaxes1 =  new Geometry.draw_multiple_axis(canvas2,context2,-t,default_axis);
        drawaxes1.draw();

        point_m=get_iso_points(x11,y11,0);
    
        var b:number=point_m[1][0];
    
        var d:number=point_m[0][0];

        
      
        var drawcircle1=new Geometry.Circle(d,b,10,"red",context2);
        drawcircle1.draw();
        point_f=get_iso_points(x22,y22,0);
    
        var drawcircle2=new Geometry.Circle(point_f[0][0],point_f[1][0],10,"blue",context2);
        drawcircle2.draw();

        
    }
    
    context2.restore();

    
    window.requestAnimationFrame(next2);
}

function check2(){
    if(default_axis=='Y'){
        if((parseInt(x2.value)-parseInt(y2.value))>=0){
            corr_ang=(parseInt(x2.value)-parseInt(y2.value));
        }
        else{
            corr_ang = 360-((parseInt(y2.value)-parseInt(x2.value)));
        }
    }
    else{
        if((parseInt(y2.value)-parseInt(x2.value))>=0){
            corr_ang=(parseInt(y2.value)-parseInt(x2.value));
        }
        else{
            corr_ang = 360-(parseInt(x2.value)-parseInt(y2.value));
        }
    }
    //console.log(corr_ang);

    var my_ang:HTMLInputElement=<HTMLInputElement>document.getElementById('ang');
    if(corr_ang>=(parseInt(my_ang.value)-3) && corr_ang<=(parseInt(my_ang.value)+3)){
        alert("Correct Answer");

        var nxt_btn : HTMLInputElement = <HTMLInputElement> document.getElementById('next23');
        nxt_btn.style.display = "inline-block";
    }
    else{
        alert("Wrong Answer. Try Again");
    }

}

function get_iso_points(x:number,y:number,z:number) : number[][]
{
    var x0:number=x;
    var y0:number=y;
    var z0:number=z;
        

    var matrix : number[][] = [];
    for(var i=0; i<3; i++) {
        matrix[i] = [];
        for(var j=0; j<3; j++) {
            matrix[i][j] = 0;
        }
    }

    matrix[0][0] = 0.7071;
    matrix[0][1] = 0;
    matrix[0][2] = -0.7071;
    matrix[1][0] = 0.4082;
    matrix[1][1] = 0.8165;
    matrix[1][2] = 0.4082;
    matrix[2][0] = 0.5773;
    matrix[2][1] = -0.5773;
    matrix[2][2] = 0.5773;

    var coord : number[][] = [];
    for(var i=0; i<3; i++) {
        coord[i] = [];
        for(var j=0; j<1; j++) {
            coord[i][j] = 0;
        }
    }

    coord[0][0] = x0;
    coord[1][0] = y0;
    coord[2][0] = z0;

    var iso_points: number[][] = [];

    for(let i =0;i<3;i++) {
        iso_points[i] = [];
        for(let j=0;j<1;j++) {
            iso_points[i][j] = 0;
        }
    }

    for(let i =0;i<3;i++) {
        for(let j=0;j<1;j++) {
            for(let k =0;k<3;k++) {
                iso_points[i][j] += matrix[i][k]*coord[k][j];
            }
        }
    }

    var iso: number[][] = [];
    
    for(let i =0;i<2;i++) {
        iso[i] = [];
        for(let j=0;j<1;j++) {
            iso[i][j] = 0;
        }
    }

    iso[0][0] = iso_points[0][0];
    iso[1][0] = iso_points[1][0];

    return iso;
    
}


function next23()
{
    var next_div23 :  HTMLInputElement = <HTMLInputElement> document.getElementById('div34');
    next_div23.style.display = "none";
    var di :  HTMLInputElement = <HTMLInputElement> document.getElementById('mat');
    di.style.display = "";
    s1.value+=corr_ang;
}

