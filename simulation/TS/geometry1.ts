class Axes
{
    public canvas: HTMLCanvasElement;
    public context: CanvasRenderingContext2D;
    public l: number=100;
    public m: number=25;
    public x1: number;
    public y1: number;
    public x2: number;
    public y2: number;
    public x3: number;
    public y3: number;
    
    

    constructor(canvas:HTMLCanvasElement,context:CanvasRenderingContext2D)
    {
        this.l = 100;
        this.m=25;
        this.x1 = -this.l*0.7071;
        this.y1 = -this.l*0.4082;
        this.x2 = 0;
        this.y2 = this.l*0.8166;
        this.x3 = this.l*0.7071;
        this.y3 = -this.l*0.4082;
        this.canvas = canvas;
        this.context = context;
    }

        draw()
    {
        this.context.beginPath();
        this.context.moveTo(0,0);
        this.context.lineTo(this.x1,this.y1);
        this.context.strokeStyle='red';
        this.context.stroke();
        this.context.strokeText("X",this.x1-5,this.y1-5);
        
    
        this.context.beginPath();
        this.context.moveTo(0,0);
        this.context.lineTo(this.x2,this.y2);
        this.context.strokeStyle='green';
        this.context.stroke();
        this.context.scale(1,-1);        
        this.context.strokeText("Z",this.x2+5,this.y2-170);
        //this.context.rotate(2*Math.PI);
        
        this.context.scale(1,-1);
        this.context.beginPath();
        this.context.moveTo(0,0);
        this.context.lineTo(this.x3,this.y3);
        this.context.strokeStyle='blue';
        //this.context.moveTo(this.x3-5,this.y3-5);
        this.context.scale(1,-1);
        this.context.strokeText("Y",this.x3-5,this.y3+100);
        this.context.stroke();
        
        
    }
}

