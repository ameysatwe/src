namespace Geometry {
    export class draw_multiple_axis {
        public canvas: HTMLCanvasElement;
        public context: CanvasRenderingContext2D;
        public l: number = 100;
        public m: number = 25;
        public x1: number;
        public y1: number;
        public x2: number;
        public y2: number;
        public x3: number;
        public y3: number;
        public t:number;
        public default:string; 
        
        
        constructor(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D,t:number,default_axis:string) {
            this.l = 100;
            this.m = 25;
            this.t = t;
            this.x1 = -this.l * 0.7071;
            this.y1 = -this.l * 0.4082;
            this.x2 = 0;
            this.y2 = this.l * 0.8166;
            this.x3 = this.l * 0.7071;
            this.y3 = -this.l * 0.4082;
            this.canvas = canvas;
            this.context = context;
            this.default = default_axis;
        }
        
    draw()
    {   
        var t1 = (this.t)*Math.PI/180
        
        var oldx1 = this.x1;
        var oldy1 = this.y1;
        var oldx2 = this.x2;
        var oldy2 = this.y2;
        var oldx3 = this.x3;
        var oldy3 = this.y3;

        if(this.default!=''){
            this.context.rotate(Math.PI);
        }
        

        if(this.default=='Z'){
            
            this.context.rotate(Math.PI);
            
            var x111=((this.x1*Math.cos(t1))-(this.y1*Math.sin(t1)));
            var y111=((this.y1*Math.cos(t1))+(this.x1*Math.sin(t1)));

            var x222=((this.x2*Math.cos(t1))-(this.y2*Math.sin(t1)));
            var y222=((this.y2*Math.cos(t1))+(this.x2*Math.sin(t1)));
            this.x2+=x222;
            this.y2+=y222;
            this.x1+=x111;
            this.y1+=y111;
                
              

        }

        if(this.default=='X'){
            
            var x333=((this.x3*Math.cos(t1))-(this.y3*Math.sin(t1)));
            var y333=((this.y3*Math.cos(t1))+(this.x3*Math.sin(t1)));

            var x222=((this.x2*Math.cos(t1))-(this.y2*Math.sin(t1)));
            var y222=((this.y2*Math.cos(t1))+(this.x2*Math.sin(t1)));
            
            this.x2+=x222;
            this.y2+=y222;

            this.x3+=x333;
            this.y3+=y333;
            
            this.context.rotate(Math.PI);
            this.context.rotate(Math.PI);
        }

        if(this.default=='Y'){
            var x111=((this.x1*Math.cos(t1))-(this.y1*Math.sin(t1)));
            var y111=((this.y1*Math.cos(t1))+(this.x1*Math.sin(t1)));

            var x333=((this.x3*Math.cos(t1))-(this.y3*Math.sin(t1)));
            var y333=((this.y3*Math.cos(t1))+(this.x3*Math.sin(t1)));
            
            this.x3+=x333;
            this.y3+=y333;
            
            this.x1+=x111;
            this.y1+=y111;
            
            //this.context.rotate(Math.PI);
        }

        
        this.context.beginPath();
        this.context.moveTo(0, this.m);
        this.context.lineTo((this.x1), (this.y1 + this.m));
        this.context.strokeStyle = 'blue';
        this.context.strokeText("W 3", this.x1 - 5, this.y1+this.m - 5);
        this.context.stroke();
        
        this.context.beginPath();
        this.context.moveTo(0, this.m);
        this.context.lineTo(this.x2, (this.y2 + this.m + this.m));
        this.context.strokeStyle = 'blue';
        this.context.strokeText("W 5", this.x2 - 5, this.y2+this.m+this.m+this.m - 5);
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(0, this.m);
        this.context.lineTo(this.x3, (this.y3 +this.m));
        this.context.strokeStyle = 'blue';
        this.context.stroke();
        this.context.strokeText("W Ɩ", this.x3 - 5, this.y3+this.m - 5);
        
        // first axes

        this.context.beginPath();
        this.context.moveTo(0, 0);
        this.context.lineTo(oldx1, oldy1);
        this.context.strokeStyle = 'red';
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.strokeText("Է 3", oldx1 - 5, oldy1 - 5);

        this.context.beginPath();
        this.context.moveTo(0, 0);
        this.context.lineTo(oldx2, oldy2 + this.m);
        this.context.strokeStyle = 'red';
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.strokeText("Է 5", oldx2+this.m*0.4 - 5, oldy2+this.m - 5);

        this.context.beginPath();
        this.context.moveTo(0, 0);
        this.context.lineTo(oldx3, oldy3);
        this.context.strokeStyle = 'red';
        this.context.lineWidth = 2;
        this.context.stroke();
        this.context.strokeText("Է Ɩ", oldx3 - 5, oldy3 - 5);

    }
}

    export class Circle {
        public _centpt_x: number;
        public _centpt_y: number;
        public _radius: number;
        public context: CanvasRenderingContext2D;
        public _color: string = "red";

        constructor(centpt_x: number, centpt_y: number, radius:number, color: string, context: CanvasRenderingContext2D) {
            this._centpt_x = centpt_x;
            this._centpt_y = centpt_y;
            this.context = context;
            this._color = color;
            this._radius = radius;
        }
        draw() {
            this.context.beginPath();
            context.moveTo(0, 0);
            this.context.arc(this._centpt_x, this._centpt_y, this._radius, 0, 2 * Math.PI);
            this.context.fillStyle = this._color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }

}
