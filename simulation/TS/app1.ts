var canvas3 = <HTMLCanvasElement>document.getElementById("mycanvas3");
var context3: CanvasRenderingContext2D = canvas3.getContext("2d");
var x1_1:number;
var x2_1:number;
var x3_1:number;
var a:number;
var b:number;
var c:number;
var trial:number = 2;
var default_axis:string;


context3.translate(canvas3.width/2,canvas3.height/2);
context3.scale(1,-1);

var drawaxes1: Axes= new Axes(canvas3,context3);
drawaxes1.draw();


function x()
{
    x1_1=1;
    x2_1=x3_1=0;
    var but = document.getElementById('xbutton');
    but.style.backgroundColor = "green"; 
    but = document.getElementById('ybutton');
    but.style.backgroundColor = "white";
    but = document.getElementById('zbutton');
    but.style.backgroundColor = "white";
    default_axis = 'X';
}

function y()
{
    x2_1=1;
    x1_1=x3_1=0;
    var but = document.getElementById('xbutton');
    but.style.backgroundColor = "white"; 
    but = document.getElementById('ybutton');
    but.style.backgroundColor = "green";
    but = document.getElementById('zbutton');
    but.style.backgroundColor = "white";
    default_axis = 'Y';
}

function z()
{
    x3_1=1;
    x1_1=x2_1=0;
    var but = document.getElementById('xbutton');
    but.style.backgroundColor = "white"; 
    but = document.getElementById('ybutton');
    but.style.backgroundColor = "white";
    but = document.getElementById('zbutton');
    but.style.backgroundColor = "green";
    default_axis = 'Z';
}
function A()
{
    a=1;
    b=c=0;
    var but = document.getElementById('abutton');
    but.style.backgroundColor = "green";
    but = document.getElementById('bbutton');
    but.style.backgroundColor = "white";
    but = document.getElementById('cbutton');
    but.style.backgroundColor = "white";
}
function B()
{
    b=1;
    a=0;
    c=0;
    var but = document.getElementById('abutton');
    but.style.backgroundColor = "white";
    but = document.getElementById('bbutton');
    but.style.backgroundColor = "green";
    but = document.getElementById('cbutton');
    but.style.backgroundColor = "white";
}
function C()
{
     c=1;
     a=0;b=0;
     var but = document.getElementById('abutton');
     but.style.backgroundColor = "white";
     but = document.getElementById('bbutton');
     but.style.backgroundColor = "white";
     but = document.getElementById('cbutton');
     but.style.backgroundColor = "green";
}

function next1()
{
    var par : HTMLInputElement = <HTMLInputElement> document.getElementById('rotmatp');
    par.style.display = "inline-block";

    var par : HTMLInputElement = <HTMLInputElement> document.getElementById('rotmatopt');
    par.style.display = "inline-block";

    var btn : HTMLInputElement = <HTMLInputElement> document.getElementById('checkbtn');
    btn.style.display = "inline-block";
}

function next12(){
    var next_div23 :  HTMLInputElement = <HTMLInputElement> document.getElementById('div34');
    next_div23.style.display = "";
    var next_div12 :  HTMLInputElement = <HTMLInputElement> document.getElementById('div12');
    next_div12.style.display = "none";
}

function check()
{
    if( x1_1==1 && a==1 || x2_1==1 && b==1 || x3_1==1 && c==1)
    {
        alert("Correct option");
        x1_1=0;x2_1=0;x3_1=0;a=0;b=0;c=0;
        
        var btn1 : HTMLInputElement = <HTMLInputElement> document.getElementById('nextbtn12');
        btn1.style.display = "inline-block";

       // var 

    }
    else
    {
            trial-=1;
            if(trial==1){
                alert("wrong option " + " :- " +  trial +" trial left");
            }
            var but = document.getElementById('abutton');
            but.style.backgroundColor = "white";
            but = document.getElementById('bbutton');
            but.style.backgroundColor = "white";
            but = document.getElementById('cbutton');
            but.style.backgroundColor = "white";
            but = document.getElementById('xbutton');
            but.style.backgroundColor = "white";
            but = document.getElementById('ybutton');
            but.style.backgroundColor = "white";
            but = document.getElementById('zbutton');
            but.style.backgroundColor = "white";

            if(trial==0){
                alert("Please go to theory and read once again!")
            }
        
        
    }
    
}


