
namespace part6
{

    export class  div6
    {
       public  default_a : string;
       public  x1:number;
       public  y1:number;
       public  x2:number ;
        public y2:number;
        public ang : number;
        public x3 : number ;
        public y3 : number;
      //  public context: CanvasRenderingContext2D

        constructor( default_axis :string,x1:number, y1:number, x2:number , y2:number, x3:number,y3:number,ang:number)
        {
            this.default_a=default_axis;
            this.x1=x1;
            this.y1=y1;
            this.x2=x2;
            this.y2=y2;
            this.y3=y3;
            this.x3=x3;
            this.ang=ang;
         //   this.context=context;
        } 
    calculate1()
    {
        var t1=(<HTMLInputElement>document.getElementById("t1"));
        var t2=(<HTMLInputElement>document.getElementById("t2"));
        var t3=(<HTMLInputElement>document.getElementById("t3"));
        var t4=(<HTMLInputElement>document.getElementById("t4"));
        var t5=(<HTMLInputElement>document.getElementById("t5"));
        var t6=(<HTMLInputElement>document.getElementById("t6"));
        var t7=(<HTMLInputElement>document.getElementById("t7"));
        var t8=(<HTMLInputElement>document.getElementById("t8"));
        var t9=(<HTMLInputElement>document.getElementById("t9"));
       
     //   let ans=( <HTMLParagraphElement>document.getElementById("ans"));
      
        var i;
        var j;
     //   m[1][1] 
        var m =[];
if(this.default_a=='X')
{
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }
        m[0][0] = 1 ;
        m[0][1] = 0 ;
        m[0][2] = 0  ;
        m[1][0] = 0;
        m[1][1] = (Math.cos(Math.PI / 180 * this.ang).toFixed(1));
        m[1][2] =-(Math.sin(Math.PI / 180 * this.ang).toFixed(1));
        m[2][0] = 0;
        m[2][1] =(Math.sin(Math.PI / 180 * this.ang).toFixed(1));
        m[2][2] =( Math.cos(Math.PI / 180 * this.ang).toFixed(1));


        ((<HTMLInputElement>document.getElementById("t1")).value)=m[0][0].toString();
        ((<HTMLInputElement>document.getElementById("t2")).value)=m[0][1].toString();
        ((<HTMLInputElement>document.getElementById("t3")).value)=m[0][2].toString(); 
        ((<HTMLInputElement>document.getElementById("t4")).value)=m[1][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t5")).value)=m[1][1].toString();
        ((<HTMLInputElement>document.getElementById("t6")).value)=m[1][2].toString();
        ((<HTMLInputElement>document.getElementById("t7")).value)=m[2][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t8")).value)=m[2][1].toString(); 
        ((<HTMLInputElement>document.getElementById("t9")).value)=m[2][2].toString();  
     
    } 
    else if(this.default_a=='Y')
    {
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }

        m[0][0] = (Math.cos(Math.PI / 180 * this.ang).toFixed(1)) ;
        m[0][1] = 0;
        m[0][2] =  (Math.sin(Math.PI / 180 * this.ang).toFixed(2));
        m[1][0] = 0;
        m[1][1] = 1 ;
        m[1][2] =0;
        m[2][0] =  -(Math.sin(Math.PI / 180 * this.ang).toFixed(2));
        m[2][1] =0;
        m[2][2] =   (Math.cos(Math.PI / 180 * this.ang).toFixed(2));

        ((<HTMLInputElement>document.getElementById("t1")).value)=m[0][0].toString();
        ((<HTMLInputElement>document.getElementById("t2")).value)=m[0][1].toString();
        ((<HTMLInputElement>document.getElementById("t3")).value)=m[0][2].toString(); 
        ((<HTMLInputElement>document.getElementById("t4")).value)=m[1][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t5")).value)=m[1][1].toString();
        ((<HTMLInputElement>document.getElementById("t6")).value)=m[1][2].toString();
        ((<HTMLInputElement>document.getElementById("t7")).value)=m[2][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t8")).value)=m[2][1].toString(); 
        ((<HTMLInputElement>document.getElementById("t9")).value)=m[2][2].toString();  
     
    } 
    else if(this.default_a=='Z')
    {
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }

        m[0][0] = (Math.cos(Math.PI / 180 * this.ang).toFixed(1)) ;
        m[0][1] = -(Math.sin(Math.PI / 180 * this.ang).toFixed(1)) ;
        m[0][2] =  0 ;
        m[1][0] = ( Math.sin(Math.PI / 180 * this.ang).toFixed(1));
        m[1][1] =  ( Math.cos(Math.PI / 180 * this.ang).toFixed(1)) ;
        m[1][2] =0;
        m[2][0] =0;
        m[2][1] =0;
        m[2][2] =1 ;

        ((<HTMLInputElement>document.getElementById("t1")).value)=m[0][0].toString();
        ((<HTMLInputElement>document.getElementById("t2")).value)=m[0][1].toString();
        ((<HTMLInputElement>document.getElementById("t3")).value)=m[0][2].toString(); 
        ((<HTMLInputElement>document.getElementById("t4")).value)=m[1][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t5")).value)=m[1][1].toString();
        ((<HTMLInputElement>document.getElementById("t6")).value)=m[1][2].toString();
        ((<HTMLInputElement>document.getElementById("t7")).value)=m[2][0].toString(); 
        ((<HTMLInputElement>document.getElementById("t8")).value)=m[2][1].toString(); 
        ((<HTMLInputElement>document.getElementById("t9")).value)=m[2][2].toString();  
     
           
}

    }
}
}