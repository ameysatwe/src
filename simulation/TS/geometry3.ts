
namespace part5
{
    export class  div5
    {
       public  default_axis :string;
        public corr_ang : number;

        constructor( default_axis :string, corr_ang:number)
        {
            this.default_axis=default_axis;
            this.corr_ang=corr_ang;
        } 

      
    calculate1()
    {

        var t11=(<HTMLInputElement>document.getElementById("t1"));
        var t2=(<HTMLInputElement>document.getElementById("t2"));
        var t3=(<HTMLInputElement>document.getElementById("t3"));
        var t4=(<HTMLInputElement>document.getElementById("t4"));
        var t5=(<HTMLInputElement>document.getElementById("t5"));
        var t6=(<HTMLInputElement>document.getElementById("t6"));
        var t7=(<HTMLInputElement>document.getElementById("t7"));
        var t8=(<HTMLInputElement>document.getElementById("t8"));
        var t9=(<HTMLInputElement>document.getElementById("t9"));
        var t10=(<HTMLInputElement>document.getElementById("t10"));
        var s1=(<HTMLInputElement>document.getElementById("s1"));
       
    
    
       

       var a1 = parseFloat(t11.value);
        var a2 = parseFloat(t2.value);
        var a3 = parseFloat(t3.value);
        var a4 = parseFloat(t4.value);
        var a5 = parseFloat(t5.value);
        var a6 = parseFloat(t6.value);
        var a7 = parseFloat(t7.value);
        var a8 = parseFloat(t8.value);
        var a9 = parseFloat(t9.value);

        t1_n.value="";
        t2_n.value="";
        t3_n.value="";
        t4_n.value="";
        t5_n.value="";
        t6_n.value="";
        t7_n.value="";
        t8_n.value="";
        t9_n.value="";

        t1_n.value+=a1;
        t2_n.value+=a2;
        t3_n.value+=a3;
        t4_n.value+=a4;
        t5_n.value+=a5;
        t6_n.value+=a6;
        t7_n.value+=a7;
        t8_n.value+=a8;
        t9_n.value+=a9;
      
        var i;
        var j;
        var m =[];
    if(this.default_axis=='X')
    {
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }

        m[0][0] = 1 ;
        m[0][1] = 0 ;
        m[0][2] = 0 ;
        m[1][0] = 0 ;
        m[1][1] = (Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2));
        //console.log(m[1][1]);
        m[1][2] =-(Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2));
        //console.log(m[1][2]);
        m[2][0] = 0;
        m[2][1] =(Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2));
        //console.log(m[2][1]);
        m[2][2] =( Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2));
        //console.log(m[2][2]);


        if(m[0][0]==a1 && m[0][1]==a2 &&  m[0][2]==a3 && m[1][0]==a4 && (Math.abs(m[1][1]-a5)<=0.1) && (Math.abs(m[1][2]-a6)<=0.1)&& m[2][0]==a7 && (Math.abs(m[2][1]-a8)<=0.1) && (Math.abs(m[2][2]-a9)<=0.1))
        {
         
          document.getElementById("t10").style.display="";
        }
        else
        {
            document.getElementById("t10").style.display="none";
            alert("recalculate");
        }   
}


    else if(this.default_axis=='Y')
{
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }

        m[0][0] = (Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2)) ;
       //console.log(m[0][0]);
        m[0][1] = 0;
        m[0][2] =  (Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2));
      //  console.log(m[0][2]);
        m[1][0] = 0;
        m[1][1] = 1 ;
        m[1][2] =0;
        m[2][0] =  -(Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2));
      //  console.log(m[2][0]);
        m[2][1] =0;
        m[2][2] =   (Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2));
     //   console.log(m[2][2]);
     //console.log(m[2][0]-0.1<a7&&m[2][0]+0.1>a7);
     //console.log(m[0][0]-0.1<a1&&m[0][0]+0.1>a1);
       
      if( (Math.abs(m[0][0]-a1)<=0.1) && (m[0][1]==a2) && (Math.abs(m[0][2]-a3)<=0.1)&& (m[1][0]==a4) && (m[1][1]==a5) &&(m[1][2]==a6) && (Math.abs(m[2][0]-a7)<=0.1) && (m[2][1]==a8) &&  (Math.abs(m[2][2]-a9)<0.1))
        {
          
           document.getElementById("t10").style.display="";
        }
        else
        {
            alert("recalculate");
            document.getElementById("t10").style.display="none";
        }  
}

    else if(this.default_axis=='Z')
{
        for (i = 0; i <3; i++) 
        {
            m[i] = [];
            for ( j = 0; j < 3; j++) 
            {
                m[i][j] = 0;
            }
        }

        m[0][0] = (Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2)) ;
        //console.log(m[0][0]);
        m[0][1] = -(Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2)) ;
        //console.log(m[0][1]);
        m[0][2] =  0 ;
        m[1][0] = ( Math.sin(Math.PI / 180 * this.corr_ang).toFixed(2));
        m[1][1] =  ( Math.cos(Math.PI / 180 * this.corr_ang).toFixed(2)) ;
        m[1][2] =0;
        m[2][0] =0;
        m[2][1] =0;
        m[2][2] =1 ;
      //  console.log(a1);
      //  console.log(a2);
      //  console.log(m[0][0]-0.1<a1||m[0][0]+0.1>a1);
      //  console.log(m[0][1]-0.1<a2||m[0][1]+0.1>a2);


        if( (Math.abs(m[0][0]-a1)<=0.1) && (Math.abs(m[0][1]-a2)<=0.1) &&  (m[0][2]==a3) && (Math.abs(m[1][0]-a4)<=0.1) && (Math.abs(m[1][1]-a5)<=0.1) && (m[1][2]==a6) && (m[2][0]==a7) && (m[2][1]==a8) &&  (m[2][2]==a9))
        {

           document.getElementById("t10").style.display="";

        }
        else
        {
           alert("recalculate");
           document.getElementById("t10").style.display="none";

        }  
     
}
}
}
} 
    

   

